package configs

import (
	env "github.com/alfarih31/nb-go-env"
)

type server struct {
	URL string
}

var Server = new(server)

func (u *server) Load(env env.Env) error {

	if v, err := env.GetString("SERVER_URL", "localhost:50051"); err != nil {
		return err
	} else {
		u.URL = v
	}

	return nil
}
