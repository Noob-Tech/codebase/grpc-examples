package configs

import (
	env "github.com/alfarih31/nb-go-env"
	parser "github.com/alfarih31/nb-go-parser"
)

var Env env.Env

func Init(envFile ...string) (err error) {
	ef := parser.GetOptStringArg(envFile, ".env")

	Env, err = env.LoadEnv(ef, true)
	if err != nil {
		return
	}

	if err = Server.Load(Env); err != nil {
		return err
	}

	return
}
