# Go gRPC Client

Go gRPC Client

## Prerequisites

1. Go `v1.17`
2. Make (Optional)

## Set-up

### Install Dependencies

### Configure

#### Configure API

1. Copy [.env.example](.env.example) as [.env](.env)

```shell
cp .env.example .env
```

2. Below is available configurations:

| Key          | Description   | Values                             | Required |
|--------------|---------------|------------------------------------|---|
| `SERVER_URL` | Go server url | String, Default: `localhost:50051` | ✓ |

### Development

#### Local Runner

1. Using `Make`

```shell
make dev
```

2. Using `go cli`

```shell
go run ./main.go
```

### Contributors ###

- Alfarih Faza <alfarihfz@gmail.com>

