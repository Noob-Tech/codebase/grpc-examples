package main

import (
	"context"
	MathPB "gitlab.com/Noob-Tech/codebase/grpc-examples/go/client/api/go_pb/Math"
	"gitlab.com/Noob-Tech/codebase/grpc-examples/go/client/configs"
	"gitlab.com/Noob-Tech/codebase/grpc-examples/go/client/internal/comps"
	"gitlab.com/Noob-Tech/codebase/grpc-examples/go/client/internal/comps/server"
	"log"
)

func setup() error {
	err := configs.Init(".env")
	if err != nil {
		return err
	}

	if err = comps.Init(); err != nil {
		return err
	}

	return nil
}

func ops(ctx context.Context) {
	// Sum ops
	sIn := &MathPB.SumArg{
		Nums: []int64{10, 12, 28, 3, 17},
	}

	// Call Sum Func
	if rS, err := server.MathSvc.Sum(ctx, sIn); err != nil {
		log.Println(err)
	} else {
		log.Printf("%v, %v", sIn.String(), rS.String())
	}

	// Factorial ops
	fIn := &MathPB.FactorialArg{
		N: 8,
	}

	// Call Factorial Func
	if rF, err := server.MathSvc.Factorial(ctx, fIn); err != nil {
		log.Println(err)
	} else {
		log.Printf("%v, %v", fIn.String(), rF.String())
	}
}

func main() {
	if err := setup(); err != nil {
		log.Fatal(err)
	}

	ctx := context.Background()
	ops(ctx)
}
