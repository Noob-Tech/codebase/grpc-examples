package server

import (
	MathPB "gitlab.com/Noob-Tech/codebase/grpc-examples/go/client/api/go_pb/Math"
	"gitlab.com/Noob-Tech/codebase/grpc-examples/go/client/configs"
	"google.golang.org/grpc"
	"google.golang.org/grpc/resolver"
	"net"
	"strings"
)

var MathSvc MathPB.MathSvcClient

func Init() error {
	cfg := configs.Server

	opts := []grpc.DialOption{
		grpc.WithInsecure(),
		grpc.WithBlock(),
	}

	if !strings.Contains(cfg.URL, "localhost") {
		ipAddr := net.ParseIP(cfg.URL)
		// consider URL is hostname if ipAddr is nil
		if ipAddr == nil {
			resolver.SetDefaultScheme("dns")
			opts = append([]grpc.DialOption{grpc.WithDefaultServiceConfig(`{"loadBalancingConfig": [{"round_robin":{}}]}`)}, opts...)
		}
	}

	conn, err := grpc.Dial(cfg.URL, opts...)
	if err != nil {
		return err
	}

	MathSvc = MathPB.NewMathSvcClient(conn)
	return nil
}
