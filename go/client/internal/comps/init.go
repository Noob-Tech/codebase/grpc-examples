package comps

import (
	"gitlab.com/Noob-Tech/codebase/grpc-examples/go/client/internal/comps/server"
)

func Init() error {
	if err := server.Init(); err != nil {
		return err
	}

	return nil
}
