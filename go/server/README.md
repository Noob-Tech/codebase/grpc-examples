# Go gRPC Server

Go gRPC Server

## Prerequisites

1. Go `v1.17`
2. Make

## Set-up

### Install Dependencies

### Configure

#### Configure API

1. Copy [.env.example](.env.example) as [.env](.env)

```shell
cp .env.example .env
```

2. Below is available configurations:

|Key|Description|Values| Required |
|---|---|---|---|
| `GRPC_SERVER_PORT` | gRPC server running port | Int, Default: `50051` | ✓ |

### Development

#### Local Runner

1. Using `Make`

```shell
make dev
```

2. Using `go cli`

```shell
go run ./cmd/app/main.go
```

### Contributors ###

- Alfarih Faza <alfarihfz@gmail.com>

