package main

import (
	"context"
	"gitlab.com/Noob-Tech/codebase/grpc-examples/go/server/pkg/app"
	"log"
)

func main() {
	// Setup App
	if err := app.Setup(); err != nil {
		log.Fatal(err)
	}

	// Create context
	ctx := context.Background()

	// Start App
	if err := app.Start(ctx); err != nil {
		log.Fatal(err)
	}
}
