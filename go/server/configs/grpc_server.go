package configs

import "github.com/alfarih31/nb-go-env"

type gRPCServer struct {
	Port int
}

var GRPCServer = new(gRPCServer)

func (s *gRPCServer) Load(env env.Env) error {
	if grpcPort, err := env.GetInt("GRPC_SERVER_PORT", 50051); err != nil {
		return err
	} else {
		s.Port = grpcPort
	}

	return nil
}
