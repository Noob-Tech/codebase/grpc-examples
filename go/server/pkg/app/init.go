package app

import (
	"context"
	"gitlab.com/Noob-Tech/codebase/grpc-examples/go/server/configs"
	"gitlab.com/Noob-Tech/codebase/grpc-examples/go/server/pkg/app/presenter"
)

func Start(ctx context.Context) error {
	return presenter.Start(ctx)
}

func Setup() error {
	err := configs.Init(".env")
	if err != nil {
		return err
	}

	if err = presenter.Init(); err != nil {
		return err
	}

	return nil
}
