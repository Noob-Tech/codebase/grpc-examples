package grpc

import (
	"context"
	MathPB "gitlab.com/Noob-Tech/codebase/grpc-examples/go/server/api/go_pb/Math"
	"gitlab.com/Noob-Tech/codebase/grpc-examples/go/server/internal/svcs/math"
)

type mathPresenter struct {
	MathPB.UnimplementedMathSvcServer
}

func (p mathPresenter) Sum(ctx context.Context, arg *MathPB.SumArg) (*MathPB.SumRes, error) {
	nums := make([]int, len(arg.Nums))
	for i, n := range arg.GetNums() {
		nums[i] = int(n)
	}

	res := math.Sum(ctx, nums...)
	return &MathPB.SumRes{
		Summation: int64(res),
	}, nil
}

func (p mathPresenter) Factorial(ctx context.Context, arg *MathPB.FactorialArg) (*MathPB.FactorialRes, error) {
	res := math.Factorial(ctx, int(arg.GetN()))

	return &MathPB.FactorialRes{
		Factorial: int64(res),
	}, nil
}
