package grpc

import (
	"context"
	"fmt"
	MathPB "gitlab.com/Noob-Tech/codebase/grpc-examples/go/server/api/go_pb/Math"
	"gitlab.com/Noob-Tech/codebase/grpc-examples/go/server/configs"
	_grpc "google.golang.org/grpc"
	"log"
	"net"
)

var s *_grpc.Server

func Start(ctx context.Context) error {
	cfg := configs.GRPCServer

	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", cfg.Port))

	log.Println(ctx, "gRPC Controller running: ", lis.Addr())
	err = s.Serve(lis)
	if err != nil {
		return err
	}

	return nil
}

func Init() error {
	s = _grpc.NewServer()
	s.RegisterService(&MathPB.MathSvc_ServiceDesc, &mathPresenter{})

	return nil
}
