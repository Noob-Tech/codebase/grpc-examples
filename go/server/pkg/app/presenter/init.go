package presenter

import (
	"context"
	"gitlab.com/Noob-Tech/codebase/grpc-examples/go/server/pkg/app/presenter/grpc"
)

func Start(ctx context.Context) error {
	errChan := make(chan error)
	go func() {
		errChan <- grpc.Start(ctx)
	}()

	return <-errChan
}

func Init() error {

	if err := grpc.Init(); err != nil {
		return err
	}

	return nil
}
