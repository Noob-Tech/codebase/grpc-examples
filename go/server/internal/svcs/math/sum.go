package math

import "context"

func Sum(ctx context.Context, nums ...int) int {
	res := 0
	for _, n := range nums {
		res += n
	}

	return res
}
