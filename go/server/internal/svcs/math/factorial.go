package math

import "context"

func Factorial(ctx context.Context, n int) int {
	res := 1
	for {
		if n == 0 {
			return res
		}
		res *= n
		n--
	}
}
