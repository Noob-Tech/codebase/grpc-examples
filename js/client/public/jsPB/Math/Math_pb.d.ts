// package: math
// file: Math/Math.proto

import * as jspb from "google-protobuf";

export class SumRes extends jspb.Message {
  getSummation(): number;
  setSummation(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SumRes.AsObject;
  static toObject(includeInstance: boolean, msg: SumRes): SumRes.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SumRes, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SumRes;
  static deserializeBinaryFromReader(message: SumRes, reader: jspb.BinaryReader): SumRes;
}

export namespace SumRes {
  export type AsObject = {
    summation: number,
  }
}

export class SumArg extends jspb.Message {
  clearNumsList(): void;
  getNumsList(): Array<number>;
  setNumsList(value: Array<number>): void;
  addNums(value: number, index?: number): number;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SumArg.AsObject;
  static toObject(includeInstance: boolean, msg: SumArg): SumArg.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SumArg, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SumArg;
  static deserializeBinaryFromReader(message: SumArg, reader: jspb.BinaryReader): SumArg;
}

export namespace SumArg {
  export type AsObject = {
    numsList: Array<number>,
  }
}

export class FactorialRes extends jspb.Message {
  getFactorial(): number;
  setFactorial(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): FactorialRes.AsObject;
  static toObject(includeInstance: boolean, msg: FactorialRes): FactorialRes.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: FactorialRes, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): FactorialRes;
  static deserializeBinaryFromReader(message: FactorialRes, reader: jspb.BinaryReader): FactorialRes;
}

export namespace FactorialRes {
  export type AsObject = {
    factorial: number,
  }
}

export class FactorialArg extends jspb.Message {
  getN(): number;
  setN(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): FactorialArg.AsObject;
  static toObject(includeInstance: boolean, msg: FactorialArg): FactorialArg.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: FactorialArg, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): FactorialArg;
  static deserializeBinaryFromReader(message: FactorialArg, reader: jspb.BinaryReader): FactorialArg;
}

export namespace FactorialArg {
  export type AsObject = {
    n: number,
  }
}

