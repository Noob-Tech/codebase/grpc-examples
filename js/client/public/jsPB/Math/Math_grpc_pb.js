// GENERATED CODE -- DO NOT EDIT!

'use strict';
var grpc = require('@grpc/grpc-js');
var Math_Math_pb = require('../Math/Math_pb.js');

function serialize_math_FactorialArg(arg) {
  if (!(arg instanceof Math_Math_pb.FactorialArg)) {
    throw new Error('Expected argument of type math.FactorialArg');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_math_FactorialArg(buffer_arg) {
  return Math_Math_pb.FactorialArg.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_math_FactorialRes(arg) {
  if (!(arg instanceof Math_Math_pb.FactorialRes)) {
    throw new Error('Expected argument of type math.FactorialRes');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_math_FactorialRes(buffer_arg) {
  return Math_Math_pb.FactorialRes.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_math_SumArg(arg) {
  if (!(arg instanceof Math_Math_pb.SumArg)) {
    throw new Error('Expected argument of type math.SumArg');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_math_SumArg(buffer_arg) {
  return Math_Math_pb.SumArg.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_math_SumRes(arg) {
  if (!(arg instanceof Math_Math_pb.SumRes)) {
    throw new Error('Expected argument of type math.SumRes');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_math_SumRes(buffer_arg) {
  return Math_Math_pb.SumRes.deserializeBinary(new Uint8Array(buffer_arg));
}


var MathSvcService = exports.MathSvcService = {
  sum: {
    path: '/math.MathSvc/Sum',
    requestStream: false,
    responseStream: false,
    requestType: Math_Math_pb.SumArg,
    responseType: Math_Math_pb.SumRes,
    requestSerialize: serialize_math_SumArg,
    requestDeserialize: deserialize_math_SumArg,
    responseSerialize: serialize_math_SumRes,
    responseDeserialize: deserialize_math_SumRes,
  },
  factorial: {
    path: '/math.MathSvc/Factorial',
    requestStream: false,
    responseStream: false,
    requestType: Math_Math_pb.FactorialArg,
    responseType: Math_Math_pb.FactorialRes,
    requestSerialize: serialize_math_FactorialArg,
    requestDeserialize: deserialize_math_FactorialArg,
    responseSerialize: serialize_math_FactorialRes,
    responseDeserialize: deserialize_math_FactorialRes,
  },
};

exports.MathSvcClient = grpc.makeGenericClientConstructor(MathSvcService);
