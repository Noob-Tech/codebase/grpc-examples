// package: math
// file: Math/Math.proto

var Math_Math_pb = require("../Math/Math_pb");
var grpc = require("@improbable-eng/grpc-web").grpc;

var MathSvc = (function () {
  function MathSvc() {}
  MathSvc.serviceName = "math.MathSvc";
  return MathSvc;
}());

MathSvc.Sum = {
  methodName: "Sum",
  service: MathSvc,
  requestStream: false,
  responseStream: false,
  requestType: Math_Math_pb.SumArg,
  responseType: Math_Math_pb.SumRes
};

MathSvc.Factorial = {
  methodName: "Factorial",
  service: MathSvc,
  requestStream: false,
  responseStream: false,
  requestType: Math_Math_pb.FactorialArg,
  responseType: Math_Math_pb.FactorialRes
};

exports.MathSvc = MathSvc;

function MathSvcClient(serviceHost, options) {
  this.serviceHost = serviceHost;
  this.options = options || {};
}

MathSvcClient.prototype.sum = function sum(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(MathSvc.Sum, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

MathSvcClient.prototype.factorial = function factorial(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(MathSvc.Factorial, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

exports.MathSvcClient = MathSvcClient;

