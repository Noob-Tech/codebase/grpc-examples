// package: math
// file: Math/Math.proto

import * as Math_Math_pb from "../Math/Math_pb";
import {grpc} from "@improbable-eng/grpc-web";

type MathSvcSum = {
  readonly methodName: string;
  readonly service: typeof MathSvc;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof Math_Math_pb.SumArg;
  readonly responseType: typeof Math_Math_pb.SumRes;
};

type MathSvcFactorial = {
  readonly methodName: string;
  readonly service: typeof MathSvc;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof Math_Math_pb.FactorialArg;
  readonly responseType: typeof Math_Math_pb.FactorialRes;
};

export class MathSvc {
  static readonly serviceName: string;
  static readonly Sum: MathSvcSum;
  static readonly Factorial: MathSvcFactorial;
}

export type ServiceError = { message: string, code: number; metadata: grpc.Metadata }
export type Status = { details: string, code: number; metadata: grpc.Metadata }

interface UnaryResponse {
  cancel(): void;
}
interface ResponseStream<T> {
  cancel(): void;
  on(type: 'data', handler: (message: T) => void): ResponseStream<T>;
  on(type: 'end', handler: (status?: Status) => void): ResponseStream<T>;
  on(type: 'status', handler: (status: Status) => void): ResponseStream<T>;
}
interface RequestStream<T> {
  write(message: T): RequestStream<T>;
  end(): void;
  cancel(): void;
  on(type: 'end', handler: (status?: Status) => void): RequestStream<T>;
  on(type: 'status', handler: (status: Status) => void): RequestStream<T>;
}
interface BidirectionalStream<ReqT, ResT> {
  write(message: ReqT): BidirectionalStream<ReqT, ResT>;
  end(): void;
  cancel(): void;
  on(type: 'data', handler: (message: ResT) => void): BidirectionalStream<ReqT, ResT>;
  on(type: 'end', handler: (status?: Status) => void): BidirectionalStream<ReqT, ResT>;
  on(type: 'status', handler: (status: Status) => void): BidirectionalStream<ReqT, ResT>;
}

export class MathSvcClient {
  readonly serviceHost: string;

  constructor(serviceHost: string, options?: grpc.RpcOptions);
  sum(
    requestMessage: Math_Math_pb.SumArg,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: Math_Math_pb.SumRes|null) => void
  ): UnaryResponse;
  sum(
    requestMessage: Math_Math_pb.SumArg,
    callback: (error: ServiceError|null, responseMessage: Math_Math_pb.SumRes|null) => void
  ): UnaryResponse;
  factorial(
    requestMessage: Math_Math_pb.FactorialArg,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: Math_Math_pb.FactorialRes|null) => void
  ): UnaryResponse;
  factorial(
    requestMessage: Math_Math_pb.FactorialArg,
    callback: (error: ServiceError|null, responseMessage: Math_Math_pb.FactorialRes|null) => void
  ): UnaryResponse;
}

