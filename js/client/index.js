require('dotenv').config()

const pbSvc = require("./public/jsPB/Math/Math_grpc_pb");
const {SumArg, FactorialArg} = require("./public/jsPB/Math/Math_pb");
const grpc = require("@grpc/grpc-js");

const SERVER_URL= process.env.SERVER_URL || '0.0.0.0:50051';

function callSum(client) {
    return new Promise((resolve, reject) => {
        const arg = new SumArg()
        arg.setNumsList([10, 12, 28, 3, 17]);

        client.sum(arg, (err, res) => {
            if(err) {
                return reject(err)
            }
            resolve(`Sum of ${arg} is ${res}`)
        })
    })
}

function callFactorial(client) {
    return new Promise((resolve, reject) => {
        const arg = new FactorialArg();
        arg.setN(8)

        client.factorial(arg, (err, res) => {
            if(err) {
                return reject(err)
            }
            resolve(`Factorial of ${arg} is ${res}`)
        })
    })
}

function main() {
    const client = new pbSvc.MathSvcClient(SERVER_URL, grpc.credentials.createInsecure())

    callSum(client).then((res) => {
        console.log(res)
    })

    callFactorial(client).then((res) => {
        console.log(res)
    })
}

main();