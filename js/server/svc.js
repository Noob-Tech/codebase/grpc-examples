/**
 *
 * @param {number[]} nums
 * @returns number
 */
function sum (nums) {
  let s = 0
  nums.forEach((n) => {
    s += n
  })

  return s
}

/**
 * @param {number} n
 * @returns {number}
 */
function factorial (n) {
  let f = 1
  for (let i = n; i > 0; i--) {
    f = f * i
  }

  return f
}

module.exports = {
  sum,
  factorial
}
