# NodeJS gRPC Server

NodeJS gRPC Server

## Prerequisites

1. NodeJS `v14`

## Set-up

### Install Dependencies

### Configure

#### Configure API

1. Copy [.env.example](.env.example) as [.env](.env)

```shell
cp .env.example .env
```

2. Below is available configurations:

|Key|Description|Values| Required |
|---|---|---|---|
| `GRPC_SERVER_PORT` | gRPC server running port | Int, Default: `50051` | ✓ |

### Development

#### Local Runner

1. Using `npm`

```shell
npm start
```

2. Using `node`

```shell
node index.js
```

### Contributors ###

- Alfarih Faza <alfarihfz@gmail.com>

