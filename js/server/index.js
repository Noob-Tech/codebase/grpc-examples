require('dotenv').config()

const pbSvc = require('./public/jsPB/Math/Math_grpc_pb')
const { FactorialRes, SumRes } = require('./public/jsPB/Math/Math_pb')
const grpc = require('@grpc/grpc-js')
const { sum, factorial } = require('./svc')

// eslint-disable-next-line no-unused-vars
const { ServerUnaryCall, sendUnaryData } = require('@grpc/grpc-js')

// eslint-disable-next-line no-unused-vars
const { FactorialArg, SumArg } = require('./public/jsPB/Math/Math_pb')

const PORT = process.env.GRPC_SERVER_PORT || '50051'

/**
 * @param {ServerUnaryCall<SumArg>} call
 * @param {sendUnaryData<SumRes>} callback
 */
function handleSum (call, callback) {
  const s = sum(call.request.getNumsList())
  const res = new SumRes()
  res.setSummation(s)

  callback(null, res)
}

/**
 * @param {ServerUnaryCall<FactorialArg>} call
 * @param {sendUnaryData<FactorialRes>} callback
 */
function handleFactorial (call, callback) {
  const f = factorial(call.request.getN())
  const res = new FactorialRes()

  res.setFactorial(f)

  callback(null, res)
}

function main () {
  const server = new grpc.Server()

  server.addService(pbSvc.MathSvcService, {
    sum: handleSum,
    factorial: handleFactorial
  })

  server.bindAsync(`0.0.0.0:${PORT}`, grpc.ServerCredentials.createInsecure(), () => {
    console.log(`Server listening on: 0.0.0.0:${PORT}`)
    server.start()
  })
}

main()
