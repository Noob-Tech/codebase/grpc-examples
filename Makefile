ifndef $(GOPATH)
	GOPATH=$(shell go env GOPATH)
	export GOPATH
endif

.PHONY: help
help: ## Show this help
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

pre-log:
	@echo "Generating proto..."

post-log:
	@echo "Completed"

.PHONY: proto-go proto-js proto-ts proto-py
all: proto-go proto-js proto-ts proto-py ## Generate all proto to all lang from .proto


.PHONY: proto-go
proto-go: pre-log ## Generate Go grpc pb from .proto
	@mkdir -p go/server/api/go_pb
	@rm -rf go/server/api/go_pb/*
	@protoc -I=./proto --go_out=./go/server/api/go_pb/ --go_opt=paths=source_relative \
		 --go-grpc_out=./go/server/api/go_pb/ --go-grpc_opt=paths=source_relative \
		 --proto_path=proto \
		 proto/**/*
	 @make post-log

.PHONY: proto-js
proto-js: pre-log ## Generate JS grpc pb from .proto
	@mkdir -p js/server/public/jsPB
	@rm -rf js/server/public/jsPB/*
	@grpc_tools_node_protoc -I=./proto \
		--ts_out ./js/server/public/jsPB/ \
		--ts_opt output_javascript \
		--proto_path=proto \
		proto/**/*
	@make post-log

.PHONY: proto-ts
proto-ts: pre-log ## Generate TS grpc pb from .proto
	@mkdir -p gen/ts
	@rm -rf gen/ts/*
	@protoc -I=./proto \
		--ts_out=./gen/ts/ \
		--ts_opt generate_dependencies \
		--proto_path=proto \
		proto/**/*
	@make post-log


.PHONY: proto-py
proto-py: pre-log ## Generate Py grpc pb from .proto
	@mkdir -p py/server/api/py_pb
	@find py/server/api/py_pb ! -name 'setup.py' -type d -mindepth 1 -exec rm -rf {} +
	@find py/server/api/py_pb ! -name 'setup.py' -type f -mindepth 1 -exec rm -rf {} +
	@python3 -m grpc_tools.protoc -I=./proto \
			--grpc_python_out=./py/server/api/py_pb/ \
			--python_out=./py/server/api/py_pb/ \
			--mypy_out=./py/server/api/py_pb/ \
			--proto_path=proto proto/**/*
	@find py/server/api/py_pb  -type d -maxdepth 1 -exec echo {}/__init__.py \; | xargs touch
	@make post-log
