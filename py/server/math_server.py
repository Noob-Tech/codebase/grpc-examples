import grpc
import numpy as np
from concurrent import futures
from Math import Math_pb2, Math_pb2_grpc

from dotenv import dotenv_values

# load config
config = dotenv_values(".env")

class MathSvc(Math_pb2_grpc.MathSvcServicer):
    def Sum(self,request:Math_pb2.SumArg, context):
        num = request.Nums 
        summation = sum(num)
        return Math_pb2.SumRes(Summation=summation)

    def Factorial(self, request, context):
        n = request.N
  
        def factorial(n):
            temp_status = 1
            if n<0:
                temp_status = -1
                n = abs(n)

            if n == 0:
                return 1
            else:
                return temp_status*factorial(n-1)*(n)
        return Math_pb2.FactorialRes(Factorial=factorial(n))


def serve(config):
    port = config.get('PORT')
    worker = config.get('WORKER')
    if port in [None,'',' ']:
        port = '50051'
    
    if worker in [None,'',' ']:
        worker = 1
    print(f'Server start in port {port} with {worker} worker')
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=int(worker)))
    Math_pb2_grpc.add_MathSvcServicer_to_server(MathSvc(), server)
    server.add_insecure_port('[::]:50051')
    server.start()
    server.wait_for_termination()

if __name__ == '__main__':
    serve(config)