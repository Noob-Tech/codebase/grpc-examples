import yaml
import os

def read_yaml_file(filename):
    if os.path.exists(filename) is not True:
        return {}

    with open(filename) as f:
        my_dict = yaml.safe_load(f)
        return my_dict