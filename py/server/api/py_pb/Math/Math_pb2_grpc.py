# Generated by the gRPC Python protocol compiler plugin. DO NOT EDIT!
"""Client and server classes corresponding to protobuf-defined services."""
import grpc

from Math import Math_pb2 as Math_dot_Math__pb2


class MathSvcStub(object):
    """Missing associated documentation comment in .proto file."""

    def __init__(self, channel):
        """Constructor.

        Args:
            channel: A grpc.Channel.
        """
        self.Sum = channel.unary_unary(
                '/math.MathSvc/Sum',
                request_serializer=Math_dot_Math__pb2.SumArg.SerializeToString,
                response_deserializer=Math_dot_Math__pb2.SumRes.FromString,
                )
        self.Factorial = channel.unary_unary(
                '/math.MathSvc/Factorial',
                request_serializer=Math_dot_Math__pb2.FactorialArg.SerializeToString,
                response_deserializer=Math_dot_Math__pb2.FactorialRes.FromString,
                )


class MathSvcServicer(object):
    """Missing associated documentation comment in .proto file."""

    def Sum(self, request, context):
        """Missing associated documentation comment in .proto file."""
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')

    def Factorial(self, request, context):
        """Missing associated documentation comment in .proto file."""
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')


def add_MathSvcServicer_to_server(servicer, server):
    rpc_method_handlers = {
            'Sum': grpc.unary_unary_rpc_method_handler(
                    servicer.Sum,
                    request_deserializer=Math_dot_Math__pb2.SumArg.FromString,
                    response_serializer=Math_dot_Math__pb2.SumRes.SerializeToString,
            ),
            'Factorial': grpc.unary_unary_rpc_method_handler(
                    servicer.Factorial,
                    request_deserializer=Math_dot_Math__pb2.FactorialArg.FromString,
                    response_serializer=Math_dot_Math__pb2.FactorialRes.SerializeToString,
            ),
    }
    generic_handler = grpc.method_handlers_generic_handler(
            'math.MathSvc', rpc_method_handlers)
    server.add_generic_rpc_handlers((generic_handler,))


 # This class is part of an EXPERIMENTAL API.
class MathSvc(object):
    """Missing associated documentation comment in .proto file."""

    @staticmethod
    def Sum(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/math.MathSvc/Sum',
            Math_dot_Math__pb2.SumArg.SerializeToString,
            Math_dot_Math__pb2.SumRes.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)

    @staticmethod
    def Factorial(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/math.MathSvc/Factorial',
            Math_dot_Math__pb2.FactorialArg.SerializeToString,
            Math_dot_Math__pb2.FactorialRes.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)
