import grpc
from Math import Math_pb2, Math_pb2_grpc

from dotenv import dotenv_values

# load config
config = dotenv_values(".env")

def call_sum(stub):
    in_num = Math_pb2.SumArg(Nums=[5,4,5])
    response = stub.Sum(in_num)
    return response.Summation

def call_factorial(stub):
    in_n = Math_pb2.FactorialArg(N=-10)
    response = stub.Factorial(in_n)
    return response.Factorial

def run(config):
    # NOTE(gRPC Python Team): .close() is possible on a channel and should be
    # used in circumstances in which the with statement does not fit the needs
    # of the code.
    url = config.get('SERVER_URL')
    if url in [None,'',' ']:
        url = 'localhost:50051'
    print(f'Client connect {url} succes\n')
    with grpc.insecure_channel(url) as channel:
        stub = Math_pb2_grpc.MathSvcStub(channel)
        try:
            sumres = call_sum(stub)
            factorialres = call_factorial(stub)
            print(f"math client received:\n\t summation {sumres}\t factorial {factorialres}")
        except Exception as e:
            print("ERROR >>> ",e)


if __name__ == '__main__':
    run(config)