# gRPC Examples

## Pre
1. protobuf-compiler: [Installation](https://grpc.io/docs/protoc-installation/)
2. Make

### Node
1. ts-protoc-gen: [Installation](https://www.npmjs.com/package/ts-protoc-gen)
2. grpc-tools [Installation](https://www.npmjs.com/package/grpc-tools)

### Go
1. protoc-gen-go-grpc [Installation](https://grpc.io/docs/languages/go/quickstart/#prerequisites)


### TS
1. protoc-gen-ts: [Installation](https://www.npmjs.com/package/protoc-gen-ts)

### Py
1. mypy-protobuf [Installation](https://pypi.org/project/mypy-protobuf/)
2. grpcio [Installation](https://pypi.org/project/grpcio/)
3. grpcio-tools [Installation](https://pypi.org/project/grpcio-tools/)

## Compile Proto

`Makefile` is provided for ease to call the proto command

### Compile for Go

```bash
make proto-go
```

### Compile for JS

```bash
make proto-js
```


### Compile for Py

```bash
make proto-py
```

### Compile for TS

```shell
make proto-ts
```


### Compile for Py

```shell
make proto-py
```

### Compile all language

```shell
make all
```

### Run the server
Follow the procedure for `lang` that you want to run. Server folders are located at `{lang}/server`

## Notes

### Notes for Go

1. Only copy the generated gRPC of Go to `${PROJECT_ROOT}/api/`
2. You need to replace all of the `$go_package` in generated gRPC go at `gen/go` to your go module name to resolve import

### Contributors ###

- Alfarih Faza <alfarihfz@gmail.com>
- Abdul Hamid